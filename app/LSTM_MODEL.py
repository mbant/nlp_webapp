import torch
from torch import nn
from torch.nn import functional as F

'''
model = LSTMForTextClassification(119547, 512, 256, 10)
or
model = LSTMForTextClassificationCPO(119547, 512, 512, 10)

and then
model.load_state_dict(torch.load('./pytorch_model.bin'))
'''

class LSTMForTextClassification(nn.Module):
    def __init__(self, vocab_size, embedding_dim, n_hidden, n_out):
        super().__init__()
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.n_hidden = n_hidden
        self.n_out = n_out

        self.emb = nn.Embedding(self.vocab_size, self.embedding_dim)
        self.emb_drop = nn.Dropout(0.1)
        self.lstm = nn.LSTM(input_size=self.embedding_dim, hidden_size=self.n_hidden,
                            num_layers=2,dropout=0.1, bidirectional=True,
                            batch_first=True)
        self.out = nn.Linear(self.n_hidden*6, self.n_out)
        
    # def forward(self, input_ids, attention_mask, token_type_ids, labels):
    def forward(self, input_ids):
        
        inputs = input_ids
        batch_size, seq_len = inputs.shape
        embs = self.emb_drop(self.emb(inputs))
        lstm_out, (h_n, c) = self.lstm(embs)

        lstm_out = lstm_out.view(batch_size, seq_len, 2, self.n_hidden).permute(2,0,3,1)
        h_n = h_n.view(2, 2, batch_size, self.n_hidden)[-1] #save just last layer

        avg_pool_1 = F.adaptive_avg_pool1d(lstm_out[0],1).squeeze(-1)
        avg_pool_2 = F.adaptive_avg_pool1d(lstm_out[1],1).squeeze(-1)
        max_pool_1 = F.adaptive_max_pool1d(lstm_out[0],1).squeeze(-1)
        max_pool_2 = F.adaptive_max_pool1d(lstm_out[1],1).squeeze(-1)

        outs = self.out(
            torch.cat([h_n[0],h_n[1],avg_pool_1,avg_pool_2,
                max_pool_1,max_pool_2],dim=1))
            
        return (outs,)


class LSTMForTextClassificationCPO(nn.Module): # CatPoolOnly
    def __init__(self, vocab_size, embedding_dim, n_hidden, n_out):
        super().__init__()
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.n_hidden = n_hidden
        self.n_out = n_out

        self.emb = nn.Embedding(self.vocab_size, self.embedding_dim)
        self.lstm = nn.LSTM(input_size=self.embedding_dim, hidden_size=self.n_hidden,
                            num_layers=2,dropout=0.1, bidirectional=True,
                            batch_first=True)
        self.out = nn.Linear(self.n_hidden*4, self.n_out)
        
    def forward(self, input_ids):
        
        inputs = input_ids
        batch_size, seq_len = inputs.shape
        embs = self.emb(inputs)
        lstm_out, _ = self.lstm(embs)

        lstm_out = lstm_out.view(batch_size, seq_len, 2, self.n_hidden).permute(2,0,3,1)

        avg_pool_1 = F.adaptive_avg_pool1d(lstm_out[0],1).squeeze(-1)
        avg_pool_2 = F.adaptive_avg_pool1d(lstm_out[1],1).squeeze(-1)
        max_pool_1 = F.adaptive_max_pool1d(lstm_out[0],1).squeeze(-1)
        max_pool_2 = F.adaptive_max_pool1d(lstm_out[1],1).squeeze(-1)

        outs = self.out(
            torch.cat([avg_pool_1,avg_pool_2,
                max_pool_1,max_pool_2],dim=1))
            
        return (outs,)


class LSTMForTextClassificationSimple(nn.Module):
    def __init__(self, vocab_size, embedding_dim, n_hidden, n_out):
        super().__init__()
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.n_hidden = n_hidden
        self.n_out = n_out

        self.emb = nn.Embedding(self.vocab_size, self.embedding_dim)
        self.emb_drop = nn.Dropout(0.1)
        self.lstm = nn.LSTM(input_size=self.embedding_dim, hidden_size=self.n_hidden,
                            num_layers=2,dropout=0.1, bidirectional=True,
                            batch_first=True)
        self.out = nn.Linear(self.n_hidden*2, self.n_out)
        
    def forward(self, input_ids):
        
        inputs = input_ids
        batch_size, seq_len = inputs.shape
        embs = self.emb_drop(self.emb(inputs))
        lstm_out, (h_n, c) = self.lstm(embs)

        lstm_out = lstm_out.view(batch_size, seq_len, 2, self.n_hidden).permute(2,0,3,1)
        h_n = h_n.view(2, 2, batch_size, self.n_hidden)[-1] #save just last layer

        outs = self.out(
            torch.cat([h_n[0],h_n[1]],dim=1))
            
        return (outs,)

