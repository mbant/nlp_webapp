from starlette.applications import Starlette
from starlette.responses import HTMLResponse, JSONResponse
from starlette.staticfiles import StaticFiles
from starlette.middleware.cors import CORSMiddleware
import uvicorn, aiohttp, asyncio
from io import BytesIO
import sys
from pathlib import Path

import torch
from transformers import *
# from LSTM_MODEL import LSTMForTextClassification as LSTM
# from LSTM_MODEL import LSTMForTextClassificationCPO as LSTM
from LSTM_MODEL import LSTMForTextClassificationSimple as LSTM

config_json_url = 'URL_TO_FILE'
config_json_file_name = 'models/BERT/config.json'
transformer_model_url = 'URL_TO_FILE'
transformer_model_file_name = 'models/BERT/pytorch_model.bin'

lstm_model_url = 'URL_TO_FILE'
lstm_model_file_name = 'models/LSTM/pytorch_model.bin'

path = Path(__file__).parent

app = Starlette()
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_headers=['X-Requested-With', 'Content-Type'])
app.mount('/static', StaticFiles(directory='app/static'))

async def download_file(url, dest):
    if dest.exists(): return
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            data = await response.read()
            with open(dest, 'wb') as f: f.write(data)

async def setup_transformer():
    await download_file(config_json_url, path/config_json_file_name)
    await download_file(transformer_model_url, path/transformer_model_file_name)
    try:
        model = BertForSequenceClassification.from_pretrained(str(path/'models/BERT/')).cpu()
        model.eval()
        tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased')
        return (model,tokenizer)
    except RuntimeError as e:
        raise

async def setup_lstm():
    await download_file(lstm_model_url, path/lstm_model_file_name)
    try:
        # model = LSTM(119547, 512, 256, 10).cpu() # full
        model = LSTM(119547, 512, 512, 10).cpu()   # catpool and simple 
        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        model.load_state_dict(torch.load(path/lstm_model_file_name,map_location=device))
        model.eval()
        return model
    except RuntimeError as e:
        raise

loop = asyncio.get_event_loop()
tasks_transformers = [asyncio.ensure_future(setup_transformer())]
tasks_lstm = [asyncio.ensure_future(setup_lstm())]
transformer,tokenizer = loop.run_until_complete(asyncio.gather(*tasks_transformers))[0]
lstm = loop.run_until_complete(asyncio.gather(*tasks_lstm))[0]
loop.close()

########### END PREPROCESSING

def process_text(text:str, transformer, lstm, preprocessor):
   with torch.no_grad():
      inputs = preprocessor.encode(text,return_tensors='pt',add_special_tokens=True).cpu()
      return  ( transformer(inputs)[0][0], lstm(inputs)[0][0] )


@app.route('/')
def index(request):
    html = path/'view'/'index.html'
    return HTMLResponse(html.open().read())

@app.route('/more_info')
def index(request):
    html = path/'view'/'more_info.html'
    return HTMLResponse(html.open().read())


@app.route('/analyze_text', methods=['POST'])
async def analyze_text(request):
    data = await request.form()
    text = data['text']
    pred_transformer, pred_lstm = process_text(text,transformer,lstm,tokenizer)
    return JSONResponse({
        'transformer_result': pred_transformer.argmax().item(),
        'transformer_predictions': torch.softmax(pred_transformer,0).tolist(),
        'lstm_result': pred_lstm.argmax().item(),
        'lstm_predictions': torch.softmax(pred_lstm,0).tolist()
        })

if __name__ == '__main__':
    if 'serve' in sys.argv: uvicorn.run(app=app, host='0.0.0.0', port=5042)
