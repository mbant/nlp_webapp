var el = x => document.getElementById(x);

function analyze_text() {
    var uploadText = el('text-input').value;
    if (uploadText.length < 1){
        alert('Please insert some text to analyze!');
        return;
    } 

    el('analyze-button-text').innerHTML = 'Analyzing...';

    var xhr = new XMLHttpRequest();
    var loc = window.location
    xhr.open('POST', `${loc.protocol}//${loc.hostname}:${loc.port}/analyze_text`, true);
    xhr.onerror = function() {alert (xhr.responseText);}
    xhr.onload = function(e) {
        if (this.readyState === 4) {
            var response = JSON.parse(e.target.responseText);
            // el('result-label-text').innerHTML = `${response['result']}`;
            myTransformerChart.data.datasets[0].data = response['transformer_predictions'];
            myLSTMChart.data.datasets[0].data = response['lstm_predictions'];
            myTransformerChart.update();
            myLSTMChart.update();
        }
        el('analyze-button-text').innerHTML = 'Analyze';
    }

    var textData = new FormData();
    textData.append('text', uploadText);
    xhr.send(textData);
}

