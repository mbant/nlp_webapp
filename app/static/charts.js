document.getElementById('text-input').addEventListener('keyup', (e) => {
    if(e.which === 13 || e.which === 32)
        analyze_text();
});

const transformer_ctx = document.getElementById('transformer-chart').getContext('2d');
const myTransformerChart = new Chart(transformer_ctx, {
    type: 'horizontalBar',
    data: {
        labels: ["English",
                "French",
                "Chinese Mandarin",
                "Russian",
                "Polish",
                "Portugese",
                "Japanese",
                "Italian",
                "Hebrew",
                "Dutch"],
        datasets: [{
            label: 'Model Predictions',
            data: [Math.random(), Math.random(), Math.random(), Math.random(),
                    Math.random(), Math.random(), Math.random(), 
                    Math.random(), Math.random(), Math.random()],
            backgroundColor: [
                '#e40c2b60',
                '#1d1d2c60',
                '#01FF7060',
                '#0074D960',
                '#43894560',
                '#FF851B60',
                '#5626c460',
                '#e6057660',
                '#2cccc360',
                '#facd3d60'
            ],
            borderColor: [
                '#e40c2bFF',
                '#1d1d2cFF',
                '#01FF70FF',
                '#0074D9FF',
                '#438945FF',
                '#FF851BFF',
                '#5626c4FF',
                '#e60576FF',
                '#2cccc3FF',
                '#facd3dFF'
            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
            display: true,
            text: 'BERT'
        },
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

const LSTM_ctx = document.getElementById('lstm-chart').getContext('2d');
const myLSTMChart = new Chart(LSTM_ctx, {
    type: 'horizontalBar',
    data: {
        labels: ["English",
                "French",
                "Chinese Mandarin",
                "Russian",
                "Polish",
                "Portugese",
                "Japanese",
                "Italian",
                "Hebrew",
                "Dutch"],
        datasets: [{
            label: 'Model Predictions',
            data: [Math.random(), Math.random(), Math.random(), Math.random(),
                    Math.random(), Math.random(), Math.random(), 
                    Math.random(), Math.random(), Math.random()],
            backgroundColor: [
                '#e40c2b60',
                '#1d1d2c60',
                '#01FF7060',
                '#0074D960',
                '#43894560',
                '#FF851B60',
                '#5626c460',
                '#e6057660',
                '#2cccc360',
                '#facd3d60'
            ],
            borderColor: [
                '#e40c2bFF',
                '#1d1d2cFF',
                '#01FF70FF',
                '#0074D9FF',
                '#438945FF',
                '#FF851BFF',
                '#5626c4FF',
                '#e60576FF',
                '#2cccc3FF',
                '#facd3dFF'
            ],
            borderWidth: 1
        }]
    },
    options: {
        title: {
            display: true,
            text: 'LSTM'
        },
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
