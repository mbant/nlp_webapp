## Visualizing prediction for NLP Models

This repository present a Demo Web Application where you can observe how your model reacts to inputs at "test time" or rather, in the real world!
Additionally, this is a "canvas" to try and compare different models, which might have different trade-offs in terms of accuracy or computational complexity, which can help you visually decide which model is best for you.

The case study I've chosen is pretty simple but  rather interesting: **Language Detection**. Write a few words in the text area and see how different model predict wih different accuracies in which language (out of the 10 under considerations) you're writing.

We trained/finetuned on the [Ling10 dataset](https://github.com/johnolafenwa/Ling10) the following models:
* a [multilingual BERT model](https://github.com/google-research/bert/blob/master/multilingual.md) (using the [🤗Transformers](https://huggingface.co) interface)
* a simple [BiLSTM](app/LSTM_MODEL.py) trained *only* on the Ling10 dataset, without Language Model pretraining)
<!--* a the same BiLSTM with LM pretraining or distilled from BERT -->
<!--* a Naive Bayes classifier -->

To build a more fair comparison, we've used the [same BERT-style tokenizer](https://github.com/google-research/bert/blob/master/multilingual.md#tokenization) for all models.
Note that while character-level information is probably useful for language classification, languages based on logograms ( Chinese, Japanese and Korean in our case ) are effectively encoded entirely as unigrams in our vocab, so we include that kind of information. This is a demo application anyway so a better representation is outside the scope of the repo.

The outcome is that for data the resemble the original dataset (short sentences, with previously seen words) all the models predict with high accuracy the correct language;

<img src="images/good.gif" width="800" />

For unseen words (not out-of-vocab but not present in training set) the models that were pretrained using a Language Model win however hands down, being able to relate the new query to something else which is close-by in its embedding space!

<img src="images/bad.gif" width="800" />

So the tradeoff in computational complexity will have to be weighted with how similar do you expect the incoming queries will be to the one seen in the training set in this case!

This work was heavily inspired by [AutoCat 🚘🐱](https://autocat.apps.allenai.org/), where you can also train your own model and much more!

I build this clone using [Starlette ✨](https://www.starlette.io/) for the backend in Python and [Chart.js ](https://www.chartjs.org/) for visualizing the graphs.
Starter code and styling taken from [here](https://github.com/render-examples/fastai-v3), thansk to Simon Willson.

## Running instructions

To run locally you can simply run

```bash
git clone git@gitlab.com:mbant/nlp_webapp
python3 -m pip install --user --upgrade -r requirements.txt
python3 app/server.py serve
```
(preferably in your own `virtualenv / venv`)

Alternatively you can run it in a Docker container with the provided `Dockerfile` by running

```bash
git clone git@gitlab.com:mbant/nlp_webapp
cd nlp_webapp
docker build .
docker run --name language_class_webapp -p 5042:5042 IMAGE_ID
```

<!-- TODO -->
<!-- * add timings for models (maybe even a graph comparing them?) -->
<!-- * add Naive Bayes classifier, to get an even faster/simpler model -->
<!-- * add a Distilled / pretrained multilingual BiLSTM model ? -->


